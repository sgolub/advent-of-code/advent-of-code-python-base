"""Base Advent of Code Solution Classes"""
import contextvars
import typing as t


class AdventOfCode:
    """Base class for an Advent of Code solution.

    Args:
        sample: The sample data for this solution.
        data: The real user input data for this solution
        use_sample: Should the solution be run with sample data or not
    """
    year = None
    day = None

    def __init__(self, sample: t.Any, data: t.Any, use_sample: bool = True):
        self._sample = sample
        self._data = data
        self.use_sample = use_sample

        self.current_solution = contextvars.ContextVar("current_solution", default=None)

    def parse_data(self, data: t.Any) -> str:
        """Parse the data to a workable format.

        Most of the time this method should be overridden in the sub-class for each solution.
        It returns the `data` param that is passed in by default.

        Args:
            data: The data to be used for this execution of the solution.

        Returns:
            The data to be used by the solution.
        """
        return data

    @property
    def data(self) -> t.Any:
        """The parsed data."""
        if self.use_sample:
            return self.parse_data(self._sample)
        return self.parse_data(self._data)

    async def solution_1(self):
        """Part 1 Solution"""
        raise NotImplementedError

    async def solution_2(self):
        """Part 2 Solution"""
        raise NotImplementedError

    def __str__(self):
        return f"Year {self.year} Day {self.day}"
