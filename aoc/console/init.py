"""AOC Project Initialization"""
import os
from pathlib import Path

import click
import tomli
import tomli_w


@click.command(help="Use to initialize a new module directory for AoC solutions")
@click.option("-p", "--git-push", is_flag=True, default=True,
              help="Should the results of this init be pushed up to git?")
@click.pass_context
def init(ctx, git_push: bool):
    cwd = Path.getcwd()
    target_path = Path(ctx.obj["base-dir"]).expanduser().absolute()
    year = ctx.obj['year']
    year_path = target_path.joinpath(f"year{year}")
    if not year_path.exists():
        year_path.mkdir()

    year_init_path = Path(year_path).joinpath("__init__.py")
    year_init_path.touch()

    tomlpath = target_path.joinpath("pyproject.toml")
    if not tomlpath.exists():
        os.chdir(year_path)
        os.system("poetry init --name aoc-modules")

    toml_data = {}
    with open(tomlpath, "rb") as f:
        toml_data = tomli.load(f)
    toml_data["tool"]["poetry"]["version"] = "0.1.0"

    projects = toml_data.get('tool', {})
    entry_points = projects.get('poetry', {})
    plugin_entry_points = entry_points.get('plugins', {})
    aoc_entry_points = plugin_entry_points.get('advent_of_code', {})

    aoc_entry_points[f"aoc_{year}"] = f"year{year}"

    plugin_entry_points['advent_of_code'] = aoc_entry_points
    entry_points['plugins'] = plugin_entry_points
    projects['poetry'] = entry_points
    toml_data['tool'] = projects

    pkgs = projects['poetry'].get('packages', [])

    projects['poetry']['packages'] = pkgs + [{'include': f"year{year}"}]

    with open(tomlpath, "wb+") as f:
        tomli_w.dump(toml_data, f)

    git = ctx.obj["git"]
    git.init()
    git.add("pyproject.toml", year_path)

    git.commit(f"new-year: {year}")

    if git_push:
        git.push()

    os.system(f"poetry remove {year_path} || echo 'Not installed'")
    os.system(f"poetry add -e {year_path}")

    os.chdir(cwd)
