"""AOC Day Creation/Bootstrapping"""
from datetime import datetime
import os

from ..utils import collect_input

import click


@click.command(help="Create a new day solution from the templates")
@click.option("-d", "--day", type=int, default=lambda: datetime.now().day)
@click.pass_context
def new(ctx, day):
    year = ctx.obj['year']
    padded_day = str(day).zfill(2)
    full_path = ctx.obj["base-dir"]
    year_path = full_path.joinpath(f"year{year}")
    day_path = year_path.joinpath(f"day{day}.py")
    if not year_path.exists():
        os.mkdir(year_path)

    migrations_path = ctx.obj["migrations-dir"]
    if not migrations_path.exists():
        os.mkdir(migrations_path)

    db = ctx.obj["sqlite-db"]

    migration_num = db.get_next_migration_num(migrations_path)
    migration_path = migrations_path.joinpath(f"{migration_num:03d}_add_day_{year}_{day}.sql")

    sample_data = collect_input("Enter Sample Data", inline_editor=ctx.obj['inline-editor'])
    real_data = collect_input("Enter Actual Data", inline_editor=ctx.obj['inline-editor'])

    sdata_statement = db.add_data(year, day, True, sample_data, return_statement=True)
    rdata_statement = db.add_data(year, day, False, real_data, return_statement=True)

    db.write_migration_file(migration_path, [sdata_statement, rdata_statement])

    code_template = f"""from aoc import AdventOfCode


class AOCYear{year}Day{day}(AdventOfCode):
    year = {year}
    day = {day}

    def parse_data(self, data):
        return data

    async def solution_1(self):
        \"\"\"

        .. runcmd:: poetry run aoc --year {year} run -d{day} -s1 --real
            :caption: Part 1 Solution
        \"\"\"
        raise NotImplementedError

    async def solution_2(self):
        \"\"\"

        .. runcmd:: poetry run aoc --year {year} run -d{day} -s2 --real
            :caption: Part 2 Solution
        \"\"\"
        raise NotImplementedError"""
    if not os.path.exists(day_path):
        with open(day_path, "w+") as f:
            f.write(code_template)

    lines = [
        "# AUTO-GENERATED FILE -- DO NOT MODIFY",
        f'"""The year {year} module."""',
        "from . import (",
    ] + sorted([
        f"    {entry}," for entry in os.listdir(year_path)
        if os.path.isdir(os.path.join(year_path, entry)) and entry.startswith("day")
    ]) + [")"]
    with open(os.path.join(year_path, "__init__.py"), "w+") as f:
        f.write('\n'.join(lines))

    git = ctx.obj["git"]
    git.add(year_path)
    git.commit(f"new-day: {year}-{day}")
