"""AOC Day Execution"""
from datetime import datetime
import json
import signal
import sys
import traceback

import click
import trio

from ..utils import get_day
from ..utils.console import metadata


async def _run_with_metadata(method, sol_n, ctx):
    md = None
    output = []
    with metadata(method, ctx.obj["show_docs"]) as m:
        md = m
        try:
            await m.run()
        except NotImplementedError:
            output.append(click.style(f"Solution {sol_n} Not Implemented Yet", fg="yellow", bold=True))
        except Exception as e:
            result = click.style(f"Solution {sol_n} failed: {type(e).__name__}: {e!s}", fg="red")
            if ctx.obj['with-exception']:
                result += "\n" + click.style(traceback.format_exc(), fg="red")
            output.append(result)
    output.extend(md.output)
    return output


async def _run_day(ctx, day_class, sample, solution, send_chan: trio.MemorySendChannel):
    output = [f"Running {day_class.year} Day {day_class.day}"]
    if sample:
        output.append(click.style("***Using Sample Data***", fg="yellow"))

    data = ctx.obj["sqlite-db"].get_data(ctx.obj["year"], day_class.day)

    real_data = [
        d.value if not d.value_is_json else json.loads(d.value)
        for d in data if not d.is_sample
    ][0]
    sample_data = [
        d.value if not d.value_is_json else json.loads(d.value)
        for d in data if d.is_sample
    ]

    if len(sample_data) == 1:
        sample_data = sample_data[0]
    day = day_class(sample_data, real_data, sample)
    for solution_num in [solution] if solution is not None else [1, 2]:
        day.current_solution.set(solution_num)
        output.extend(await _run_with_metadata(getattr(day, f"solution_{solution_num}"), solution_num, ctx))
        day.current_solution.set(None)
    async with send_chan:
        await send_chan.send('\n'.join(output))


async def _collate_results(recv_chan, nursery, task_status = trio.TASK_STATUS_IGNORED):
    async with recv_chan:
        task_status.started()
        results = []
        async for val in recv_chan:
            results.append(val)
        click.echo('\n'.join(sorted(results)))
    nursery.cancel_scope.cancel()


async def _run_days(ctx, days, sample, solution):
    async with trio.open_nursery() as nursery:
        send_chan, recv_chan = trio.open_memory_channel(0)
        async with send_chan:
            for d in days:
                nursery.start_soon(_run_day, ctx, d, sample, solution, send_chan.clone())
        await nursery.start(_collate_results, recv_chan, nursery)


@click.command()
@click.option("-d", "--day", default=None, multiple=True, type=click.IntRange(1, 25), help="which day's solution to run")
@click.option("-s", "--solution", default=None, type=click.IntRange(1, 2), help="which solution of each day to run")
@click.option("--sample/--real", default=True, type=bool, help="whether or not to use the sample or real data")
@click.pass_context
def run(ctx, day, solution, sample):
    """Used to run a specific day or multiple days."""
    year = ctx.obj['year']

    # all days if none specified
    if not day:
        last_day = datetime.now().day if year == datetime.now().year else 25
        day = list(range(1, last_day+1))

    if not day:
        raise ValueError("no days determined")
    days = []
    for d in day:
        try:
            mod = get_day(year, d)
        except KeyError as e:
            if ctx.obj['with-exception']:
                raise e
            click.secho(f"No module for day {d} of year {year} found", fg="red")
            sys.exit(1)
        except AttributeError as e:
            if ctx.obj['with-exception']:
                raise e
            click.secho(f"No module for day {d} of year {year} found", fg="red")
        else:
            days.append(getattr(mod, f"AOCYear{year}Day{d}"))

    trio.run(
        _run_days,
        ctx,
        days,
        sample,
        solution,
        restrict_keyboard_interrupt_to_checkpoints=True,
    )
