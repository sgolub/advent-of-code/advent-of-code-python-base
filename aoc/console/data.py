from pathlib import Path
import sys

import click
import inquirer
from sqlalchemy.sql import text as satext

from ..utils import collect_input


def _valid_day(answers, current):
    current = int(current)
    if current < 1 or current > 25:
        raise inquirer.errors.ValidationError("", reason="Day must be between 1 and 25")
    return True


@click.group(help="Managed data for a day")
@click.option(
    "-d",
    "--day",
    type=int,
)
@click.option(
    "--sample/--real",
    default=None,
    type=bool,
)
@click.pass_context
def data(ctx, day, sample):
    ctx.ensure_object(dict)
    if ctx.invoked_subcommand == "import":
        return
    if day is None:
        day = inquirer.text(
            "Which day is this data for?",
            validate=_valid_day,
        )
    ctx.obj["day"] = int(day)
    ctx.obj["sample"] = sample


@data.command(help="Insert New Data")
@click.pass_context
def insert(ctx):
    sample = ctx.obj["sample"]
    if sample is None:
        sample = inquirer.confirm(
            "Is this sample data?",
            default=True,
        )

    data = collect_input("Enter Data", inline_editor=ctx.obj["inline-editor"])
    db = ctx.obj["sqlite-db"]
    migration_num = db.get_next_migration_num(ctx.obj['migrations-dir'])
    year = ctx.obj['year']
    day = ctx.obj['day']
    stmt = db.add_data(ctx.obj['year'], ctx.obj['day'], sample, data, return_statement=True)
    migration_name = f"{migration_num:03d}_insert_data_{year}_{day}.sql"
    d = ctx.obj['migrations-dir'].joinpath(migration_name)
    db.write_migration_file(d, [stmt])


@data.command(help="List Data")
@click.pass_context
def list(ctx):
    data = ctx.obj["sqlite-db"].get_data(ctx.obj['year'], ctx.obj['day'])
    result = inquirer.prompt([
        inquirer.List(
            "id",
            message="Select Data",
            choices=[
                (f"{d.id} - {'Sample' if d.is_sample else 'Real'} Data", i)
                for i, d in enumerate(data)
            ],
        ),
        inquirer.List(
            "action",
            message="What would you like to do?",
            choices=[
                "View",
                "Edit",
                "Delete",
            ],
        ),
        inquirer.Confirm(
            "confirm",
            message="Are you sure?",
            default=False,
            ignore=lambda answers: answers["action"] != "Delete",
        ),
    ])
    if not result:
        return

    match result["action"]:
        case "View":
            click.echo(data[result['id']].value)
        case "Edit":
            new_data = click.edit(data[result['id']].value)
            if new_data is not None:
                with ctx.obj["sqlite-db"].session as session:
                    data[result['id']].value = new_data
                    session.commit()
        case "Delete":
            if result["confirm"]:
                click.echo(f"Deleting {data[result['id']].id}")
                ctx.obj["sqlite-db"].delete_data(data[result['id']].id)
            else:
                click.echo("Canceling Delete")


@data.command(help="Delete Data")
@click.argument("data-id", type=int)
@click.pass_context
def delete(ctx, data_id):
    try:
        ctx.obj["sqlite-db"].delete_data(data_id)
    except Exception as e:
        click.echo(f"Failed to delete data {data_id}: {e!s}", err=True)
    else:
        click.echo(f"Deleted Data {data_id}")


@data.command(name="import", help="Import Migrations")
@click.pass_context
def import_(ctx):
    migrations_path = ctx.obj["migrations-dir"]
    if not migrations_path.exists() or not migrations_path.is_dir():
        click.echo(f"Invalid migration path {migrations_path}", err=True)
        sys.exit(1)

    sql_files = sorted([p for p in migrations_path.iterdir() if p.suffix == ".sql"])

    db = ctx.obj['sqlite-db']
    click.echo("Creating database tables")
    db.create_all()

    click.echo("Populating from migrations")
    with db.engine.begin() as conn:
        db_conn = conn.connection
        for p in sql_files:
            click.echo(f"... executing script: {p!s}")
            with open(p) as f:
                db_conn.executescript(f.read())
