"""Console entrypoint for user interaction."""
from datetime import datetime
import os
from pathlib import Path

import click

from .init import init
from .data import data
from .new import new
from .run import run
from ..utils.db import Database
from ..utils.git import Repo


@click.group(name="aoc")
@click.option("-y", "--year", type=int, default=lambda: datetime.now().year)
@click.option("-d", "--show-docs", is_flag=True, default=False)
@click.option("-t", "--with-exception", is_flag=True, default=False)
@click.option(
    "-b",
    "--base-dir",
    type=click.Path(exists=True),
    default="./",
    help="The base directory to run solutions from",
)
@click.option(
    "--sqlite-db",
    type=click.Path(
        file_okay=True,
        dir_okay=False,
        writable=True,
    ),
    default=lambda: str(Path.cwd().joinpath("aoc.db")),
    help="The path to the SQLite database file"
)
@click.option(
    "--encryption-key",
    type=str,
    default="aoc",
    help="The encryption key to use for the SQLite database",
    envvar="AOC_ENCRYPTION_KEY",
)
@click.option(
    "--inline/--editor",
    default=True,
    type=bool,
    help="",
)
@click.pass_context
def cli(
    ctx,
    year,
    show_docs,
    with_exception,
    sqlite_db,
    encryption_key,
    base_dir,
    inline,
):
    """A CLI tool for managing advent-of-code mechanics."""
    ctx.ensure_object(dict)

    if year > datetime.now().year:
        click.secho(f"{year} is greater than current time... No time traveling allowed.", fg="red")
        ctx.exit(1)
    ctx.obj["year"] = year
    ctx.obj["show_docs"] = show_docs
    ctx.obj["with-exception"] = with_exception
    ctx.obj["base-dir"] = Path(base_dir).expanduser().absolute()
    ctx.obj["migrations-dir"] = ctx.obj['base-dir'].joinpath("migrations")
    db = Database(sqlite_db)
    if not Path(sqlite_db).exists:
        click.secho(f"Creating SQLite database at {sqlite_db}", fg="green")
        db.create_all()
    ctx.obj["sqlite-db"] = db
    ctx.obj["git"] = Repo(base_dir)
    ctx.obj["inline-editor"] = inline

    os.environ["AOC_ENCRYPTION_KEY"] = encryption_key


cli.add_command(init)
cli.add_command(data)
cli.add_command(new)
cli.add_command(run)
