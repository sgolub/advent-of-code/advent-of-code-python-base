"""Module that has utility methods for working with the console."""
from datetime import datetime
import inspect
import typing as t

import click


class metadata:
    """A contextual manager for capturing statistics and outputting formatted data.

    Args:
        f: The method to run. Can be a coroutine.
        show_docs: should the docstrings of the method be output to the console?
    """
    def __init__(self, f: t.Callable, show_docs: bool = False):
        self.f = f
        self.show_docs = show_docs
        self.doc = self.f.__doc__
        self.name = f.__name__
        self.name = ' '.join(map(str.title, f.__name__.split("_")))
        self.result = None
        self.output: t.List[str] = []

    def __enter__(self):
        self.output.append(f"Running {self.name}")
        if self.show_docs and self.doc:
            lines = self.doc.splitlines()
            max_length = max(map(len, lines))
            horiz = '~'*(max_length+2)
            self.output.append(f"+{horiz}+")
            for line in lines:
                self.output.append(f"| {line:{max_length}} |")
            self.output.append(f"+{horiz}+")
        self.start_time = datetime.now()
        return self

    async def run(self):
        """Run the method passed in for this context."""
        self.result = self.f()
        if inspect.iscoroutine(self.result):
            self.result = await self.result

    def __exit__(self, exc_type, exc_value, traceback):
        total_time = datetime.now() - self.start_time
        self.output.append(click.style(f" Result: {self.result}", bold=True, fg="green"))
        self.output.append(click.style(f"  took: {total_time}", fg="yellow"))

    def echo(self):
        """Echo current buffered output to the console."""
        click.echo('\n'.join(self.output))
