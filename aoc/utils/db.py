from functools import cached_property
import json
from pathlib import Path
import typing as t

from sqlalchemy import create_engine, insert, select
from sqlalchemy.orm import Session

from ..models import Base, Data


class Database:
    def __init__(self, url: str):
        url = str(url)
        if not url.startswith("sqlite://"):
            url = f"sqlite:///{url}"
        self._url = url

    @cached_property
    def engine(self):
        return create_engine(self._url)

    def create_all(self):
        Base.metadata.create_all(self.engine)

    @property
    def session(self):
        return Session(self.engine)

    def get_data_by_id(self, id: int) -> t.Optional[Data]:
        with self.session as session:
            return session.get(Data, id)

    def delete_data(self, id: int):
        with self.session as session:
            data = session.get(Data, id)
            session.delete(data)
            session.commit()

    def get_data(self, year: int, day: int, is_sample: t.Optional[bool] = None) -> t.Optional[t.List[Data]]:
        conditions = (Data.year == year) & (Data.day == day)

        if is_sample is not None:
            conditions &= (Data.is_sample == is_sample)

        stmt = select(Data).where(conditions)
        with self.session as session:
            return session.execute(stmt).scalars().all()

    def get_data_add_statement(self, year: int, day: int, is_sample: bool, value: str):
        value_is_json = False
        if not isinstance(value, str):
            value = json.dumps(value)
            value_is_json = True

        return insert(Data).values(
            year=year,
            day=day,
            is_sample=is_sample,
            value=value,
            value_is_json=value_is_json,
        )

    def add_data(self, year: int, day: int, is_sample: bool, value: str, return_statement: bool = False):
        with self.session as session:
            new_data = self.get_data_add_statement(year, day, is_sample, value)
            result = session.execute(new_data)
            session.commit()
            if return_statement:
                return new_data.compile(compile_kwargs=dict(literal_binds=True))
            return result

    @staticmethod
    def write_migration_file(target_path: str, migrations: list[str]):
        with open(target_path, "w+") as f:
            f.write("\n".join([f"{m};" for m in migrations]))

    @staticmethod
    def get_next_migration_num(migration_dir: Path | str) -> str:
        existing_migrations = [
            f for f in Path(migration_dir).iterdir() if f.suffix == ".sql"
        ]
        return len(existing_migrations) + 1
