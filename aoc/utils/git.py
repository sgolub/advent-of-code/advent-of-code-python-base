"""Utilities for working with git"""
import contextlib
from functools import partial
from itertools import chain
import os
import pathlib
import subprocess
import typing as t


class Repo:
    def __init__(self, path: t.Union[str, pathlib.Path], bare: bool = False):
        self.path = os.path.abspath(path)
        self.bare = bare
        if bare:
            self.init()

    @contextlib.contextmanager
    def cd(self):
        start_path = os.getcwd()
        if self.path != start_path:
            os.chdir(self.path)
        yield
        os.chdir(start_path)

    def git(self, *args, **kwargs):
        if args[0] != "git":
            args = ("git",) + args
        args = args + tuple(chain.from_iterable((f"--{k}", v,) for k, v in kwargs.items()))
        return subprocess.run(args, capture_output=True)

    def init(self, **kwargs):
        with self.cd():
            return self.git("init", **kwargs)

    def add(self, *files: t.List[t.Union[str, pathlib.Path]], **kwargs):
        self.git("add", *files, **kwargs)

    def commit(self, message: str, files: t.Optional[t.List[t.Union[str, pathlib.Path]]] = None, **kwargs):
        args: t.List[t.Any] = ["commit"]
        if files is not None:
            args.extend(files)
        args.extend(["-m", message])
        return self.git(*args, **kwargs)

    def __getattr__(self, command):
        return partial(self.git, command)
