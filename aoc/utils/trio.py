"""Utilities for working with :mod:`trio`"""
from collections import namedtuple
import typing as t

import trio


GatherResult = namedtuple("GatherResult", ("index", "result",))


async def _run_call(send_chan: trio.MemorySendChannel, index: int, call: t.Callable):
    async with send_chan:
        await send_chan.send(GatherResult(index, await call()))


async def trio_gather(calls: t.List[t.Callable]) -> t.List[t.Any]:
    """Gather results of async methods using :mod:`trio` conventions.

    Since the :mod:`trio` library is incompatible with the builtin :func:`asyncio.gather`
    method, this is provided to give the same functionality as that method while using
    conventions put forth by :mod:`trio`.

    Args:
        calls: the list of calls to await.

    Returns:
        The results of the calls in the same order they were passed in.
    """
    async with trio.open_nursery() as nursery:
        send_chan, recv_chan = trio.open_memory_channel(len(calls))
        for i, call in enumerate(calls):
            nursery.start_soon(_run_call, send_chan, i, call)

    results = []
    async for value in recv_chan:
        results.append(value)
    return list(val.result for val in sorted(results, key=lambda x: x.index))
