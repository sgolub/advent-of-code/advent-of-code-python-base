"""Utility methods"""
from functools import lru_cache
import pkg_resources

import click

from .console import metadata
from .trio import trio_gather


@lru_cache()
def get_all_years():
    eps = {}
    for entry_point in pkg_resources.iter_entry_points('advent_of_code'):
        eps[entry_point.name] = entry_point.load()
    return eps


@lru_cache()
def get_year(year):
    return get_all_years()[f"aoc_{year}"]


def get_day(year, day):
    return getattr(get_year(year), f"day{day}")


def multiline_input(prompt: str, default: str = None) -> str:
    """Get a multiline input from the user"""
    click.echo(f"{prompt} (empty line completes):")
    lines = []
    while True:
        try:
            line = input()
        except EOFError:
            break
        if not line:
            break
        lines.append(line)
    return '\n'.join(lines)


def collect_input(prompt: str, default: str = None, inline_editor: bool = True) -> str:
    if inline_editor:
        return multiline_input(prompt, default=default)
    return click.edit(default)
