from functools import partial
import os
import typing as t

import sqlalchemy as sa
from sqlalchemy.orm import Mapped, mapped_column
from sqlalchemy_utils.types.encrypted.encrypted_type import AesEngine, StringEncryptedType

from .base import Base


key_loader = partial(
    os.environ.get,
    'AOC_ENCRYPTION_KEY',
    'aoc',
)


class Data(Base):
    __tablename__ = 'data'

    id: Mapped[int] = mapped_column(primary_key=True)
    year: Mapped[int]
    day: Mapped[int]
    is_sample: Mapped[bool]
    value: Mapped[str] = mapped_column(
        StringEncryptedType(
            sa.Text,
            key_loader,
            AesEngine,
        )
    )
    value_is_json: Mapped[bool]

    def __repr__(self):
        return (
            f'<{self.__class__.__name__}(year={self.year}, '
            f'day={self.day}, is_sample={self.is_sample})>'
        )
