# Advent of Code Tooling
```{toctree}
---
titlesonly:
maxdepth: 1
---
```

The tool in this repo is meant to facilitate working with the annual coding challenge of [Advent Of Code](https://adventofcode.com).


```{runcmd} poetry run aoc --help
---
caption: aoc --help
---
```

## Subcommands

There are three sub-commands to help manage your solutions.

### init

```{runcmd} poetry run aoc init --help
---
caption: aoc init --help
---
```

### new

```{runcmd} poetry run aoc new --help
---
caption: aoc new --help
---
```
### run

```{runcmd} poetry run aoc run --help
---
caption: aoc run --help
---
```

### data

```{runcmd} poetry run aoc data --help
---
caption: aoc data --help
---
```
